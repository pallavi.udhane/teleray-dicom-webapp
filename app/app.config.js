'use strict';
angular.module('TelerayApp').
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
	$locationProvider.hashPrefix('!');
	$routeProvider.
		when('/studies', {
			template: '<study-list></study-list>'
		}).
		when('/studies/:studyId', {
			template: '<study-detail></study-detail>'
		}).
		otherwise('/studies');
}]);
