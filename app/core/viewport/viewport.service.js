'use strict';

angular.
module('core.viewport').
factory('Viewport', [function() {
	var viewportlist = [];
	function deSelectAllTools(element){
		cornerstoneTools.wwwc.disable(element);
		cornerstoneTools.pan.activate(element, 2); // 2 is middle mouse button
		cornerstoneTools.zoom.activate(element, 4); // 4 is right mouse button
		cornerstoneTools.probe.deactivate(element, 1);
		cornerstoneTools.length.deactivate(element, 1);
		cornerstoneTools.angle.deactivate(element, 1);
		cornerstoneTools.ellipticalRoi.deactivate(element, 1);
		cornerstoneTools.rectangleRoi.deactivate(element, 1);
		cornerstoneTools.stackScroll.deactivate(element, 1);
		cornerstoneTools.wwwcTouchDrag.deactivate(element);
		cornerstoneTools.zoomTouchDrag.deactivate(element);
		cornerstoneTools.panTouchDrag.deactivate(element);
		cornerstoneTools.stackScrollTouchDrag.deactivate(element);
	}
	return {
		setElement: function (element){
			viewportlist.push(element);
		},
		enableElement: function (element){
			cornerstone.enable(element);
		},
		resizeElement: function (element){
			angular.forEach(viewportlist, function(viewport){
				cornerstone.resize(viewport, true);
			});
		},
		displayImage: function(series, element, enabletools){
		
			var imageId = series.instanceList[0].imageId;
			
			var stack = {
				seriesDescription: series.seriesDescription,
				stackId: series.seriesNumber,
				imageIds: [],
				currentImageIdIndex: 0,
				frameRate: series.frameRate
			};
			angular.forEach(series.instanceList, function(image) {
				var imageId = image.imageId;
				stack.imageIds.push(imageId);
			});

			cornerstone.loadAndCacheImage(imageId).then(function(image) {
				cornerstone.displayImage(element, image);
				cornerstone.fitToWindow(element);
				if(enabletools === "true") {
					cornerstoneTools.mouseInput.enable(element);

					cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
					cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
					cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
					cornerstoneTools.probe.enable(element);
					cornerstoneTools.length.enable(element);
					cornerstoneTools.ellipticalRoi.enable(element);
					cornerstoneTools.rectangleRoi.enable(element);

					cornerstoneTools.addStackStateManager(element, ['playClip']);
					cornerstoneTools.addToolState(element, 'stack', stack);
					cornerstoneTools.stackScrollWheel.activate(element);
					cornerstoneTools.stackPrefetch.enable(element);
				}
			});
		}, enableTool: function (action){
			angular.forEach(viewportlist, function(viewport){
				if(action === 'wwwc') {
					console.log('wwc has clicked');
					deSelectAllTools(viewport);
					cornerstoneTools.wwwc.activate(viewport, 1);
				} else if(action === 'invert') {
					deSelectAllTools(viewport);
					var viewportHandle = cornerstone.getViewport(viewport);
					// Toggle invert
					if (viewportHandle.invert === true) {
						viewportHandle.invert = false;
					} else {
						viewportHandle.invert = true;
					}
					cornerstone.setViewport(viewport, viewportHandle);
				} else if(action === 'zoom') {
					deSelectAllTools(viewport);
					cornerstoneTools.zoom.activate(viewport, 5);
				} else if(action === 'pan') {
					deSelectAllTools(viewport);
					cornerstoneTools.pan.activate(viewport, 3);
				} else if(action === 'stackScroll') {
					deSelectAllTools(viewport);
					cornerstoneTools.stackScroll.activate(viewport, 1);
				} else if(action === 'length') {
					deSelectAllTools(viewport);
					cornerstoneTools.length.activate(viewport, 1);
				} else if(action === 'angle') {
					deSelectAllTools(viewport);
					cornerstoneTools.angle.activate(viewport, 1);
				} else if(action === 'probe') {
					deSelectAllTools(viewport);
					cornerstoneTools.probe.activate(viewport, 1);
				} else if(action === 'ellipticalRoi') {
					deSelectAllTools(viewport);
					cornerstoneTools.ellipticalRoi.activate(viewport, 1);
				} else if(action === 'rectangleRoi') {
					deSelectAllTools(viewport);
					cornerstoneTools.rectangleRoi.activate(viewport, 1);
				} else if(action === 'playClip') {
					deSelectAllTools(viewport);
					var stackState = cornerstoneTools.getToolState(viewport, 'stack');
					var frameRate = stackState.data[0].frameRate;
					// Play at a default 10 FPS if the framerate is not specified
					if (frameRate === undefined) {
						frameRate = 10;
					}
					cornerstoneTools.playClip(viewport, frameRate);
				} else if(action === 'stopClip') {
					deSelectAllTools(viewport);
					cornerstoneTools.stopClip(viewport);
				} else {
					deSelectAllTools(viewport);
				}
			});
		},
	};
}]);
