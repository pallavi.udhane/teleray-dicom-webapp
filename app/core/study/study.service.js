'use strict';

angular.
  module('core.study').
  factory('Study', ['$resource',
    function($resource) {
	  console.log('****************Inside Study Factory****************');
	  var url = 'http://dev.radimazer.com/teleray-service/patientstudies/searchPatientStudiesByStudyIdForDicom/:studyId';
	  
      return $resource(url, {}, {
        query: {
          method: 'GET',
          params: {studyId: 'studyList'},
          isArray: false
        }
      });
    }
  ]);
