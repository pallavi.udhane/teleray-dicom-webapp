'use strict';

angular.module('studyDetail', [
  'ngRoute',
  'core.study',
  'core.viewport'
]);
