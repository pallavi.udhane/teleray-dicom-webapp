'use strict';

angular.
module('studyDetail').
component('studyDetail', {
	templateUrl: 'study-detail/study-detail.template.html',
	controller: ['$routeParams', '$timeout', 'Study', 'Viewport','$scope','$http',
		function StudyDetailController($routeParams, $timeout, Study, Viewport , $scope , $http) {
		
		console.log('********************Inside StudyDetailController with study as *********************' +JSON.stringify(Study));
		//get study from database using studyid
		// alert('getting study id as ::' +$routeParams.studyId);

		
		 		
			var self = this;
		
			
			//getting study  as::
			//var studyId = '5947689838588217848883c1';
			self.study = Study.get({studyId: $routeParams.studyId}, function(study) {	
				//alert($routeParams.studyId);
				study = study.study;
				
				
				angular.forEach(study.seriesList, function(series) {					
				//	alert('getting seriesList as ::' +JSON.stringify(series));
					angular.forEach(series.instanceList, function(image) {
						//alert('getting instanceList as ::' +image);
						var imageId = image.imageId;
						if (image.imageId.substr(0, 4) !== 'http') {
							var strImageID = image.imageId.split('images/');
							var assignImagePath = strImageID[1];
							imageId = "dicomweb://127.0.0.1:8000/images/" + assignImagePath;
							
							
							console.log('getting image id as ::' +imageId);
							
						}
						image.imageId = imageId;
					});//angular instanceLst
				}); //angular seriesLst
				self.setSelectedStudy(study.seriesList[0]);
			//	alert('getting self setSelectedStudy as ::' +JSON.stringify(study.seriesList[0]));
				console.log('getting **** self study as ::' +JSON.stringify(self.study));
			});//study get
			
			
			self.setSelectedStudy = function setImage(series) {
				self.selectedStudy = series;
			};//setSelectedStudy
			self.toggleAction = function(action){
				Viewport.enableTool(action);
			};//toggleaction

			self.layout = [ "1x1", "1x2", "2x1", "2x2"];
			self.selectedLayout = "1x1";
			self.viewportLayout = ["top:0px;left:0px;height:100%;width:100%;"];			
			self.changeLayout = function() {
				switch (self.selectedLayout) {
					case "1x2":
						self.viewportLayout = ["left:0px;width:50%;", "right:0px;width:50%;"];
						break;
					case "2x1":
						self.viewportLayout = ["top:0px;height:50%;", "bottom:0px;height:50%;"];
						break;
					case "2x2":
						self.viewportLayout = ["top:0px;left:0px;height:50%;width:50%;", "top:0px;right:0px;height:50%;width:50%;", "bottom:0px;left:0px;height:50%;width:50%;", "bottom:0px;right:0px;height:50%;width:50%;"];
						break;
					default:
						self.viewportLayout = ["top:0px;left:0px;height:100%;width:100%;"];
						break;
				}
				$timeout(function () {
					Viewport.resizeElement();
				});
			};
		}
	]
});
