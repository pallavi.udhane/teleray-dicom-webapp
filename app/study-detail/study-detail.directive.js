angular.module('studyDetail')
.directive('dicom', ['$window', 'Viewport', function ($window, Viewport) {
	return {
        restrict: 'A',
		replace: true,
		scope: {},
        link: function (scope, element, attrs) {
			var ele = element[0];
			if(ele.children.length == 0) {
				Viewport.enableElement(ele);
				if(attrs.enabletools === "true") {
					Viewport.setElement(ele);
				}
			}
			if(attrs.enabletools === "false") {
				element.draggable({
					appendTo: 'body',
					helper: "clone"
				});
			} else {
				element.droppable({
					drop:function(event,ui) {
						var stringData = angular.element(ui.draggable).data('series');
						attrs.$set('series', stringData);
						alert('setting series data as :' +JSON.stringify(stringData));
					}
				});
			}
			attrs.$observe('series', function(stringData) {
				if(stringData) {
					var series = angular.fromJson(stringData);
					Viewport.displayImage(series, ele, attrs.enabletools);
				}
			});
			angular.element($window).bind('resize', function(){
				Viewport.resizeElement(ele);
			});
        }
    };
}]);
