'use strict';

// Declare app level module which depends on views, and components
angular.module('TelerayApp', [
  'ngRoute',
  'core',
  'studyDetail',
  'studyList'
]);
